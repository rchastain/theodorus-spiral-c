
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

struct t_vector
{
  double x;
  double y;
};

void print(struct t_vector a_vector);
struct t_vector rotate(struct t_vector a_vector, double a_theta);
struct t_vector perpendicular(struct t_vector a_vector, bool a_clockwise);
struct t_vector normalize(struct t_vector a_vector);
struct t_vector add(struct t_vector a_vector1, struct t_vector a_vector2);
struct t_vector scale(struct t_vector a_vector, double a_factor);
struct t_vector set_length(struct t_vector a_vector, double a_length);
