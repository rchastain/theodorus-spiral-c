
#include "vectors.h"

void print(struct t_vector a_vector)
{
  printf("a_vector.x = %f\n", a_vector.x);
  printf("a_vector.y = %f\n", a_vector.y);
}

struct t_vector rotate(struct t_vector a_vector, double a_theta)
{
/*
  x' = x cos θ − y sin θ
  y' = x sin θ + y cos θ
*/
  struct t_vector result;
  result.x = a_vector.x * cos(a_theta) - a_vector.y * sin(a_theta);
  result.y = a_vector.x * sin(a_theta) + a_vector.y * cos(a_theta);
  return result;
}

struct t_vector perpendicular(struct t_vector a_vector, bool a_clockwise)
{
/*
  x' = x cos θ − y sin θ
  y' = x sin θ + y cos θ
*/
  double l_sign = a_clockwise ? -1.0 : 1.0;
  struct t_vector result;
  result.x = -a_vector.y * l_sign;
  result.y =  a_vector.x * l_sign;
  return result;
}

struct t_vector normalize(struct t_vector a_vector)
{
  double l_length = sqrt(a_vector.x * a_vector.x + a_vector.y * a_vector.y);
  
  struct t_vector result;
  result.x = a_vector.x / l_length;
  result.y = a_vector.y / l_length;
  return result;
}

struct t_vector add(struct t_vector a_vector1, struct t_vector a_vector2)
{
  struct t_vector result;
  result.x = a_vector1.x + a_vector2.x;
  result.y = a_vector1.y + a_vector2.y;
  return result;
}

struct t_vector scale(struct t_vector a_vector, double a_factor)
{
  struct t_vector result;
  result.x = a_vector.x * a_factor;
  result.y = a_vector.y * a_factor;
  return result;
}

struct t_vector set_length(struct t_vector a_vector, double a_length)
{
  struct t_vector result = normalize(a_vector);
  result.x = result.x * a_length;
  result.y = result.y * a_length;
  return result;
}
