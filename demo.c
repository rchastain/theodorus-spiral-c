
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#include "vectors.h"

int main(void)
{
  struct t_vector l_vector = {2.0, 0.0};
  
  print(l_vector);
  l_vector = rotate(l_vector, M_PI);
  print(l_vector);
  l_vector = perpendicular(l_vector, false);
  print(l_vector);
  l_vector = normalize(l_vector);
  print(l_vector);
  
  return 0;
}
