
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <cairo/cairo.h>

#include "vectors.h"

#define IMG_W 480
#define IMG_H 480
#define NUM_TRI 16
#define SCALE 57

void draw_line(cairo_t *cr, struct t_vector a_vector1, struct t_vector a_vector2)
{
  struct t_vector l_vector1 = scale(a_vector1, SCALE);
  struct t_vector l_vector2 = scale(a_vector2, SCALE);
  
  cairo_move_to(cr, l_vector1.x, l_vector1.y);
  cairo_line_to(cr, l_vector2.x, l_vector2.y);
  cairo_stroke(cr);
}

void draw_triangle(cairo_t *cr, struct t_vector a_vector1, struct t_vector a_vector2, struct t_vector a_vector3)
{
  /* Dessin triangle */
  
  struct t_vector l_vector1 = scale(a_vector1, SCALE);
  struct t_vector l_vector2 = scale(a_vector2, SCALE);
  struct t_vector l_vector3 = scale(a_vector3, SCALE);
  
  cairo_move_to(cr, l_vector1.x, l_vector1.y);
  cairo_line_to(cr, l_vector2.x, l_vector2.y);
  cairo_line_to(cr, l_vector3.x, l_vector3.y);
  cairo_close_path(cr);
  
  cairo_set_source_rgba(cr, 1.000, 1.000, 0.878, 1.0);
  cairo_fill_preserve(cr);
  cairo_set_source_rgba(cr, 1.000, 0.271, 0.000, 1.0);
  cairo_stroke(cr);
  
  /* Marque angle droit */
  
  struct t_vector l_vector4 = set_length(perpendicular(l_vector2, false), 10);
  struct t_vector l_vector5 = perpendicular(l_vector4, false);
  struct t_vector l_vector6 = perpendicular(l_vector5, false);
  
  l_vector4 = add(l_vector2, l_vector4);
  
  cairo_move_to(cr, l_vector4.x, l_vector4.y);
  cairo_rel_line_to(cr, l_vector5.x, l_vector5.y);
  cairo_rel_line_to(cr, l_vector6.x, l_vector6.y);
  cairo_stroke(cr);
}

int main(void)
{
  cairo_surface_t *sf = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, IMG_W, IMG_H);
  cairo_t *cr = cairo_create(sf);
  
  cairo_set_source_rgba(cr, 1.000, 0.980, 0.804, 1.0);
  cairo_paint(cr);
  
  cairo_translate(cr, IMG_W / 2, IMG_H / 2);
  cairo_scale(cr, 1, -1); 
  cairo_set_line_width(cr, 0.5);
  cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
  
  cairo_set_source_rgba(cr, 1.000, 0.271, 0.000, 1.0);
  
  struct t_vector v1 = {0.0, 0.0};
  struct t_vector v2 = {1.0, 0.0};
  struct t_vector v3;
  
  int i;
  
  /*
  for (i = 1; i <= NUM_TRI; ++i)
  {
    v3 = add(v2, normalize(perpendicular(v2, false)));
    
    draw_line(cr, v1, v2);
    draw_line(cr, v2, v3);
    
    v2 = v3;
  }
  draw_line(cr, v3, v1);
  */
  
  for (i = 1; i <= NUM_TRI; ++i)
  {
    v3 = add(v2, normalize(perpendicular(v2, false)));
    
    draw_triangle(cr, v1, v2, v3);
    
    v2 = v3;
  }
  
  cairo_surface_write_to_png(sf, "image.png");
  
  cairo_destroy(cr);
  cairo_surface_destroy(sf);
  
  return 0;
}
